-- Makes a reading time estimate based on word count.
--
-- Sample configuration:
--
-- [plugins.reading_time]
--   file = "plugins/reading-time.lua"
--
-- [widgets.reading-time]
--   widget = "reading_time"
--   reading_speed = 350
--
--   # Where to insert the reading time estimate
--   selector = "span#reading-time"
--
--   # Where to extract the text for word count
--   content_selector = "main"
--
-- Minimum soupault version: 1.6
-- Author: Daniil Baturin
-- License: MIT

selector = config["selector"]
content_selector = config["content_selector"]
noreact_selector = config["noreact_selector"]

noreact = HTML.select_one(page, noreact_selector)
if not noreact then
    title = HTML.strip_tags(HTML.select_one(page, "title"))
    container = HTML.select_one(page, content_selector)
    git_timestamp_div = HTML.parse("<p> <a href=\"mailto:parisni@riseup.net?subject=" .. title .. "\">React ?</a> </p>")
    HTML.prepend_child(container, git_timestamp_div)
end
