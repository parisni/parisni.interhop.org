#!/usr/bin/env python3
# The gallery json subset is sent to stdin It will take the 4 first
# images and build a link cube. The css inside index.html builds the
# cube

import sys
import json
import re

input = sys.stdin.readline()
index_entries = json.loads(input)

# use thumbnail and resize to 130px
def renderImage(smallarr):
    content = """
  <div class="column">
  {img}
  </div>
""".format(img= "\n".join(smallarr[0:1]))

    result = """
    <div class="row">
    {content}
    </div>
""".format(content=content)
    return result

def renderGallery(img, title, url):
    if img:
        return "<li>{title}<a href='{url}'>{img}</a></li><hr class='posts'>".format(img=renderImage(img), title=title, url=url)


print("<ul class=\"nav\">")
for entry in sorted(index_entries, key=lambda k:k["date_time"], reverse=True):
    print(renderGallery(entry["img"], entry["title"], entry["url"]))
print("</ul>")
