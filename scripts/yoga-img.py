#!/usr/bin/env python3
# usage script/yoga-img.py <path>

import sys
from itertools import chain
import logging

def optimize(img_input, img_stem):
    import yoga.image
    logging.info("Converting %s into webp", img_input)
    yoga.image.optimize(img_input, img_stem + ".webp" , options={ "output_format": "webp", "resize": [1024, 768], })


def list_img(path):
    from pathlib import Path
    paths = Path(path)
    patterns = ["*.png", "*.jpeg", "*.jpg", "*.webp"]
    files = (p.resolve() for p in chain(*[paths.rglob(suff) for suff in patterns]))
    return {str(file): str(file.parent) + '/' + file.stem for file in files}

def filter_img(imgs):
    res = {}
    for key, value in imgs.items():
        if value + ".webp" not in imgs:
            res[key] = value
    return res



path = sys.argv[1]
for img_input, img_stem in filter_img(list_img(path)).items():
    optimize(img_input, img_stem)
