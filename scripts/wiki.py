#!/usr/bin/env python3
# The gallery json subset is sent to stdin It will take the 4 first
# images and build a link cube. The css inside index.html builds the
# cube

import sys
import json
import array

import pystache

template = """
<li>{{date_time}}  <a href="{{url}}">{{{title}}}</a> {{#tags}} <span class='w3-tag w3-red w3-tiny'>{{.}}</span> {{/tags}} </br></li>
"""

renderer = pystache.Renderer()

input = sys.stdin.readline()
index_entries = json.loads(input)

print("<ul class=\"nav\">")
for entry in sorted(index_entries, key=lambda k:k["date_time"], reverse=True):
    print(renderer.render(template, entry))
print("</ul>")
