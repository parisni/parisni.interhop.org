<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-07-15</time></span>
<span id="book-year" style="visibility:hidden;">1979</span>
<span id="book-author" style="visibility:hidden;">Douglas Adams</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;</span>
<span id="stars" style="visibility:hidden;">&starf;&starf;&starf;&starf;</span>

# Le guide du voyageur galactique

Des extraterrestres anéantissent totalement la terre ne laissant que
deux survivants, porteurs du fameux *guide du voyageur galactique*,
qui va les aider dans leurs aventures dans l´univers.

## Un drôle de vaisseau

Contre toute attente, Arthur et Ford se retrouvent embarqués sur un
vaisseau propulsé par un générateur d´improbabilité infinies. Dans ce
dernier, les évènements les moins probables se réalisent.
