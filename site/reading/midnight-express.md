<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-03-09</time></span>
<span id="book-year" style="visibility:hidden;">1977</span>
<span id="book-author" style="visibility:hidden;">William Hayes</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;">&starf;</span>

# Midnight Express

<p id="post-excerpt">
The famous movie Midnight Express was originally a book inspired by the real life of his author.

One of the significant moment happens in the psychiatric
hospital. After advancing further into a kind of cave, the narrator is
witness of a scene worthy H.P. Lovecraft, or Ernesto Sabato - "Héros
et Tombes". A procession of insane prisonners are walking slowly all
day long around a large pillar of stone. All of them are turning in
the same flow direction, but the narrator which does the inverse.
</p>
