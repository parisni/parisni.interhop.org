
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-05-24</time></span>
<span id="book-year" style="visibility:hidden;">1865</span>
<span id="book-author" style="visibility:hidden;">Lewis Carroll</span>
<span id="reading-time" style="visibility:hidden;"/>

# Alice In Wonderland

This has been written in 1865. It is related to the absurd litterature
movement (kafka, buzzati, becket, camus...) born during the second
world war. It is also related to little Nemo in slumber land written
in 1905, inspired by sleep. Winsor Mc Cay was abusing from drugs, and
Alice In Wonderland might be related to Edgar Allan Poe and other
fantastic tales.


I particularly enjoyed the absurd dialogues where logic, physic and
mathematic are negated.
