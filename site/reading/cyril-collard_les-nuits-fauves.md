
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-10-11</time></span>
<span id="book-year" style="visibility:hidden;">1989</span>
<span id="book-author" style="visibility:hidden;">Cyril Collard</span>
<span id="reading-time" style="visibility:hidden;"/>

<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>

# Les nuits fauves

Second roman de [Cyril
Collard](https://fr.wikipedia.org/wiki/Cyril_Collard), il fut adapté
en film et joué par l'auteur, primé aux César en 1993 quelques jours
après son décès du VIH. [Critique en ligne du
film](https://www.grignoux.be/dossiers/035/)

L'écriture est puissante, tranchante et foudroyante.

Le récit est ancré à Paris, avec des teintes du Maghreb. Dans un
contexte post guerre d'Algérie, de skinhead, toxicos, homosexuels,
sado-maso, travestis... entre les risques, l'innocence, les mensonges
et le poids de la naissance des tares héritées, les personnages se
croisent, se frôlent et se percutent.
