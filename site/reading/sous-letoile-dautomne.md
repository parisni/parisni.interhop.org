<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-05-29</time></span>
<span id="book-year" style="visibility:hidden;">1906</span>
<span id="book-author" style="visibility:hidden;">Knut Hamsun</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>

# Under Høststjærnen

"Sous l'étoile d´automne" raconte les aventures d´un jeune norvégien
en quète de repos intellectuel. Tour à tour flaneur, bucheron,
puisatier il passe de ferme en ferme, de compagnons en compagnes. Le
narrateur tente de cacher sa réussite sociale mais se montre inventif,
curieux et audacieux.

Ses aventures s´apparentent aux tribulations de Casanova ou de Barry
Lyndon mais avec le soin de dépeindre une nature vivifiante.
