<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-11-07</time></span>
<span id="book-year" style="visibility:hidden;">2020</span>
<span id="book-author" style="visibility:hidden;">Martin Ernststen</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;"></span>

# Faim


> Dans mon plaisir de ce beau matin, je me chantais une chansonnette, on
> sentait une vraie émotion dans ma voix et je me bouleversai aux larmes.

<center>

![](/images/gallery/faim/IMG_20211107_130514.webp)

![](/images/gallery/faim/IMG_20211107_131110.webp)

![](/images/gallery/faim/IMG_20211107_130927.webp)

![](/images/gallery/faim/IMG_20211107_130821.webp)

![](/images/gallery/faim/IMG_20211107_130715.webp)

</center>
