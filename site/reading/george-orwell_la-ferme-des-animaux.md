

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-16</time></span>
<span id="book-year" style="visibility:hidden;">1945</span>
<span id="book-author" style="visibility:hidden;">George Orwell</span>
<span id="reading-time" style="visibility:hidden;"/>
<span id="ystars" style="visibility:hidden;">&starf;&starf;&starf;&starf;</span>
<span id="stars" style="visibility:hidden;">&starf;</span>

# La ferme des animaux

> Les animaux sont tous égaux. Certains sont plus égaux que les autres.

À l´approche de sa fin, un vieux verrat se remémore les paroles d´une
vieille chanson révolutionnaire, qui exhorte les animaux à retrouver
leur indépendance vis-à-vis de l´homme. Il réunit les animaux de la
ferme et après sa plaidoirie, tous entonnent le champs de la
révolution, jusqu´à ce que le fermier remettent de l´ordre dans ce
raffut.

Les animaux s´entendent pour résister au fermier et à la première
occasion vont le chasser de la ferme. Plus tard il reviendra avec des
miliciens pour reprendre le contrôle mais les animaux adopteront une
stratégie militaire pour l´emporter: la fameuse **bataille de
l´Étable**. À l´issue de quoi les deux cochons Napoléon et Boule de
Neige seront médaillés.

La ferme sera alors rebaptisée la ferme des animaux, et sept
commandements seront promulgués ainsi que des rites hebdomadaires
autour du crâne du vieux verrat, du champs révolutionnaire au son des
fusils. Il est décidé que les contacts avec l´homme et ses dérivés
(maison, lits, outils...) soient proscrits. L´organisation sociale de
la ferme s´appuie sur les qualités de chaque catégorie d´animaux: les
cochons, les plus intelligents réfléchissent et décident de la marche
à suivre; les autre se mettent au travail.

Les deux cochons en chef ne sont pas d´accord sur un certain nombre de
choix et les autres animaux départagent lors de débats. En particulier
Napoléon milite pour renforcer la ferme tandis que Boule de Neige
souhaite propager la révolution aux autres ferme. En outre, ce dernier
propose l´édification d´un moulin qui aurait pour bénéfice d´aider les
animaux et leur octroyer une semaine de 3 jours, avec bien sûr un
travail initial de mise en place important.

Napoléon, en passe de perdre le débat en appelle a ses chiens qu´il a
élevé et fait fuir Boule de Neige de la ferme. Dès lors, Napoléon est
le seul maître et il va progressivement faire basculer la ferme à sa
façon. En premier lieu il va petit à petit modifier les sept
commandements. Il va aussi peu à peu modifier l´histoire de la ferme,
en désignant Boule de Neige comme un ennemi, responsable de toutes les
infortunes. Enfin le travail des animaux deviendra toujours plus
difficile, avec une retraite qu´aucun d´atteindra à cause du
surmenage.

Finalement le commerce reprend avec les humains et les cochons
investirons la ferme et dormirons dans les lits Finalement le commerce
reprend avec les humains et les cochons investirons la ferme et
dormirons dans les lits. Il iront jusqu´à marcher à deux pattes et
fraterniser avec les hommes qui seront élogieux à l´égard de la ferme
et dans leur capacité à faire travailler les animaux supérieure aux
homme.


Publié en 1945, la fable d´Orwell est une métaphore de la révolution
communiste et de son devenir. Marx, Lénine/Trotsky et Staline sont
sous les traîts des cochons, tandis que le cheval de trait représente
la classe ouvrière, le corbeau le Clergé et l´âne l´anarchiste. Les
hommes et les fermes avoisinantes représentent les pays européens,
monarchies, républiques, fachisme. Orwell dépeint comment l´avidité et
la soif de pouvoir peuvent s´appuyer sur la force de travail, la
confiance, la sottise et la bienveillance pour mettre en place des
totalitarismes indéboulonables.

Orwell aura relativement tôt une vision claire de la révolution russe,
relativement à beaucoup comme J.P Sartre qui soutiendront Staline
jusqu´en 1956 au moment de la répression de l´[insurection de
Budapest](https://fr.wikipedia.org/wiki/Insurrection_de_Budapest). Cependant,
les mémoires de Trotsky publiées 15 ans plus tôt n´étaient pas
équivoques en ce qui concernait le stalinisme.
