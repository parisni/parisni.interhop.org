### About:
I am working as a data-engineer and have a bio-informatician
background. I am currently focusing on big-data, natural language
processing and interoperability with a transversal interest in free
software.

![abominable homme des neiges](/images/abom_eat.gif)

Informatic takes a way too large place in my hobbies these days. When
I am far from a keyboard, I enjoy reading litterature, hiking, and
also looking for silex !


### Contact:
- **e-mail** — parisni \[at\] riseup.net
- **GPG key** — 1CF7 200A 2755 58A1 6809 3A8A 243D 75F9 AC86 3A05
- **Matrix** — @parisni:matrix.interhop.org
<!--
- **Fediverse** — parisni@social.interhop.org
- **Framagit** — [parisni](https://framagit.org/parisni)
- **Gitlab** — [parisni](https://gitlab.com/parisni)
- **Sourcehut** — [parisni](https://git.sr.ht/~parisni)
- **GitHub** — [parisni](https://github.com/parisni)
- **SOF** — [parisni](https://stackoverflow.com/users/3865083/parisni)
-->

<div id='noreact'/>
