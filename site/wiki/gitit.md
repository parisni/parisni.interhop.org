  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">wiki</span>

# Gitit

Gitit is a wiki based on git. Pages are rendered by `git cat` and
history with `git log`. This makes the wiki easy to maintain without
database. However this also makes the wiki not adapted to many users.

## Installation

gitit is broken in nix. It is also broken in arch. It works when
compiling with `stack`:

```bash
stack build --nix --nix-packages zlib
stack install
```

Then the  `gitit` executable is available.
