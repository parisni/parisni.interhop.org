  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">kubernetes</span>
<span class="w3-tag w3-red tag">docker</span>


# Kubernetes

## K3s

### Install

``` bash
sudo apt -y install open-iscsi
curl -sfL https://get.k3s.io | sh -
sudo cp /etc/rancher/k3s/k3s.yaml .kube/config
sudo kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.1.2/deploy/longhorn.yaml
```

### Stop/Start

``` bash
/usr/local/bin/k3s-killall.sh
systemctl start k3s
```

### Run together with other remote

``` bash
export KUBECONFIG=~/.kube/config-k3s

# use tools
kubectl ...
helm ...
k9s ...
```

## Helm

### Pass variables

```bash
helm install  airflow apache-airflow/airflow --namespace airflow  --values custom-values.yml
```

## Kubectl

### Volumes

```bash
# List the pvc
kubectl get pvc -n airflow
NAME                        STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
logs-airflow-worker-0       Bound    pvc-4e9fdfef-7a16-4299-b68a-89ee92a12589   100Gi      RWO            local-path     8m15s
data-airflow-postgresql-0   Bound    pvc-94c585fd-451f-4728-a790-f6b61a2f2b6e   8Gi        RWO            local-path     8m15s
redis-db-airflow-redis-0    Bound    pvc-95d2ca98-0fe3-424b-a939-6ffd23edacc7   1Gi        RWO            local-path     8m15s

# Delete one pvc
kubectl delete pvc logs-airflow-worker-0 -n airflow

# Delete ALL the pvc
kubectl delete pvc -n airflow  --all
```
