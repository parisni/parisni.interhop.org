  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">caldav</span>

# Davical

It is a caldav implementation based on php and postgresql.


## Install

The [docker image](https://github.com/heiderich/davical) works out of the box.

For example :
```
docker run -d --name davical-test -p 8095:80 datze/davical_https
```

Then you get administration access with default password: admin/12345

You can use a nginx reverse proxy on top of it, with letsencrypt to get ssl.

## Administrative

By visiting `http://<ip>:8095/` you get the admin interface.

Features:

- create users
- create group
- add user to group
- create group ressources...


## Clients

Tested with :

- evolution (linux)
- davx5 (android)

## Etar / davx5

1. in Etar, Settings > Add CalDav calendar > Login with url and user name
2. set `https://your-cal-url/caldav.php/<user>` and  user/password
3. set a unique email identifier
4. this creates a davx5 entry. Return to Etar, and the calendar should appear in the list.

## LDAP

According to the
[documentation](https://wiki.davical.org/index.php/Configuration/Authentication_Settings/LDAP),
davical can manage users and group from a ldap endpoint.

In order to inactive a user, there is two options :

1. delete the user in ldap
2. change its password from ldap

Configure the ldap in davical.php.

If no TLS, imortant to comment (this is not a boolean)

```php
$c->authenticate_hook['call'] = 'LDAP_check';
$c->authenticate_hook['config'] = array(
    'host' => 'openldap', //host name of your LDAP Server
    'port' => '389',              //port

     /* For the initial bind to be anonymous leave bindDN and passDN
        commented out */
 //DN to bind to this server enabling to perform request
    'bindDN'=> 'cn=admin,dc=example,dc=org',
 //Password of the previous bindDN to bind to this server enabling to perform request
    'passDN'=> 'admin',

    'protocolVersion' => '3', //Version of LDAP protocol to use
    'baseDNUsers'=> 'dc=example,dc=org', //where to look at valid user
    'filterUsers' => 'objectClass=inetOrgPerson', //filter which must validate a user according to RFC4515, i.e. surrounded by brackets
    'baseDNGroups' => 'ou=divisions,dc=tennaxia,dc=net', //where to look for groups
    'filterGroups' => 'objectClass=groupOfUniqueNames', //filter with same rules as filterUsers
     /** /!\ "username" should be set and "updated" must be set **/
    'mapping_field' => array("username" => "uid",
                             "updated" => "modifyTimestamp",
                             "fullname" => "cn" ,
                             "email" =>"mail"
                             ), //used to create the user based on his ldap properties
    'group_mapping_field' => array("username" => "cn",
                             "updated" => "modifyTimestamp",
                             "fullname" => "cn" ,
                             "members" =>"memberUid"
                             ), //used to create the group based on the ldap properties
     /** used to set default value for all users, will be overcharged by ldap if defined also in mapping_field **/
    'default_value' => array("date_format_type" => "E","locale" => "fr_FR"),
     /** foreach key set start and length in the string provided by ldap
         example for openLDAP timestamp : 20070503162215Z **/
    'format_updated'=> array('Y' => array(0,4),'m' => array(4,2),'d'=> array(6,2),'H' => array(8,2),'M'=>array(10,2),'S' => array(12,2)),
//    'startTLS' => false,  // Require that TLS is used for LDAP?
           // If ldap_start_tls is not working, it is probably
           // because php wants to validate the server's
           // certificate. Try adding "TLS_REQCERT never" to the
           // ldap configuration file that php uses (e.g. /etc/ldap.conf
           // or /etc/ldap/ldap.conf). Of course, this lessens security!
     'scope' => 'subtree', // Search scope to use, defaults to subtree.
                           // Allowed values: base, onelevel, subtree.

    );

  /* If there is some user you do not want to sync from LDAP, put their username in this list */
  $c->do_not_sync_from_ldap = array( 'admin' => true );

include('drivers_ldap.php');

```

## Shared collection

This applies for any collection (card/calendar)

1. create a group
2. add both users to group
3. create a collection in the group
4. grant access to that collection to users
5. bind to that collections ; /user1/boundcollection --> /group/shared-collection

The binding will emulate the collection owner are users. Then Etar calendar
allows to create event for a shared calendar.

## Access

By default, the user creation form grants anything to a Person. This leads the
latter to have full access. Don't forget to remove all of them.


## Tickets

They are used to share a calendar with an external person/tool, avoiding to
register. This allows to integrate davical into an external tool such google calendar.

```
https://<davical-domain>/public.php/<davical-user>/calendar/?ticket=<davical-ticket>
```
