  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">editor</span>

# Libreoffice

## Install with snap

```
sudo snap install libreoffice
```

## Change language with snap


1. [follow this procedure](https://askubuntu.com/a/1330980/333321)
2. [install extension](https://wiki.documentfoundation.org/Documentation/HowTo/install_extension)
3. [change the language](https://ask.libreoffice.org/t/how-do-i-change-the-default-language/26772/2)
