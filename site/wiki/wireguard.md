  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">security</span>
<span class="w3-tag w3-red tag">vpn</span>
<span class="w3-tag w3-red tag">dns</span>

# Wireguard
A VPN can be usefull in several contexts:
- secure access to internet from un-trusted networks
- secure access to private resources (ssh, local website)

## Setup wireguard
1. install wireguard on both server / client
2. choose a private ip for each
3. configure both server / client (create pub/priv keys and bellow configuration)
4. run `wg-quick up wg0` on both


Client config:
```
cat /etc/wireguard/wg0.conf
[Interface]
PrivateKey = <the client private key>
Address = <a client private ip>/32
DNS = <a local DNS server ip>

[Peer]
PublicKey = <the server public key>
Endpoint = <the server public ip>:<the server wireguard port>
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 25
```

Server config:
```
cat /etc/wireguard/wg0.conf
[Interface]
PostUp = sysctl -w net.ipv4.ip_forward=1; iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o <network interface> -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o <network interface> -j MASQUERADE
PrivateKey = <the server private key>
Address = <a server private ip>/32
ListenPort = <the server wireguard port>

[Peer]
PublicKey = <the client public key>
AllowedIPs = <a client private ip>/32 # the ip address in the VPN network of the client you just created
```

## Protect ssh with wireguard
1. configure sshd on the server with the bellow configuration
2. limit access to ssh port from the wireguard server private ip
3. disable internet access to the ssh port


sshd config:
```
cat /etc/ssh/sshd_config
ListenAddress <a server private ip>
Port 7184
PermitRootLogin no
PubkeyAuthentication yes
PasswordAuthentication no
PermitEmptyPasswords no
ChallengeResponseAuthentication no
X11Forwarding yes
```

ufw config:
```
cat /etc/ufw/applications.d/sshd
[SSHD]
title=SSHD
description=SSHD
ports=7184/tcp
```

```
ufw allow from <a client private ip>/32 to any app "SSHD"
```

## Setup a local DNS

Install and Setup `unbound`

``` bash
cat /etc/unbound/unbound.conf
server:
        chroot: ""
        num-threads: 1
        verbosity: 1
        root-hints: "/etc/unbound/root.hints"
        trust-anchor-file: "/etc/unbound/trusted-key.key"
        interface: 0.0.0.0
        max-udp-size: 3072
        access-control: 0.0.0.0/0               refuse
        access-control: 127.0.0.1               allow
        access-control: <private ip>/24         allow
        private-address: <private ip>/24
        hide-identity: yes
        hide-version: yes
        harden-glue: yes
        harden-dnssec-stripped: yes
        unwanted-reply-threshold: 10000000
        val-log-level: 1
        cache-min-ttl: 1800
        cache-max-ttl: 14400
        prefetch: yes
        prefetch-key: yes

```

Configure wireguard client with DNS (see above)

Check for dnsleak:
- [dnsleak](https://dnsleak.com/)

Check for DNSSEC:
- `unbound-host -C /etc/unbound/unbound.conf -v sigok.verteiltesysteme.net`
- `unbound-host -C /etc/unbound/unbound.conf -v sigfail.verteiltesysteme.net`


Use resolv.conf:

```
cat /etc/resolvconf.conf
name_servers=<the vpn server private ip>
resolv_conf_options="trust-ad"
```

Update resolv.conf:

```
resolvconf -u
```

## Protect local website

On the nginx side:

``` bash
# replace
listen 443 ssl;
# with
listen <a private vpn ip>:443 ssl;
```

Also for letsencrypt to use the acme-challenge (otherwise it won't succeed):

```
server {
        server_name *.domain.org;

        listen [::]:80;
        listen 80;

        location /.well-known/acme-challenge/ {
            root /var/www;
            try_files $uri =404;
        }

        access_log off;

        return 301 https://$host$request_uri;
}
```

On the OVH dns side (or whatever):

On the unbound side:

```
        local-data: "the.website.com IN A <a private vpn ip>"
```

Now nginx only provide those website to people using the VPN and the DNS redirects to it.

## Generate a qrcode for your android wireguard client

1. generate a priv/pub pair key on the server side
2. create a client conf on the server side
3. add the Peer on the server file
4. reload the wireguard service `systemctl reload wg-quick@wg0.service`
5. generate a qrcode `qrencode -t ansiutf8 -o /tmp/<user>.png -t png -r /etc/wireguard/clients/<user>.conf`


## Make sshd daemon work at reboot

The default systemd sshd requires network to start. When sshd listen to
wireguard private ip, then add those in the systemd unit file:

```
After=network.target wg-quick@wg0.service
Requires=sys-devices-virtual-net-wg0.device
```

# References
- [scaleway tutorial](https://www.scaleway.com/en/docs/tutorials/install-wireguard/)
- [terminology](https://www.procustodibus.com/blog/2020/12/wireguard-terminology/)
- [ssh jump host](https://jamesledger.net/securing-ssh-with-a-jump-host/)
- [mailing list](https://www.linuxquestions.org/questions/linux-security-4/openvpn-ssh-listenaddress-4175607641-print/)
- [protecting ssh with vpn](https://security.stackexchange.com/questions/242586/logging-into-remote-server-via-ssh-using-vpn-wireguard-is-it-foolproof)
- [dnsleak](https://dnsleak.com/)
- [unbound archlinux](https://wiki.archlinux.org/title/Unbound)
- [unbound local](https://www.asrivas.me/blog/configuring-unbound-as-a-local-dns-server/)
- [vpn setup](https://www.ckn.io/blog/2017/11/14/wireguard-vpn-typical-setup/)
- [generate qrcode](https://serversideup.net/generating-wireguard-qr-codes-for-fast-mobile-deployments/)
- [sshd after wireguard](https://unix.stackexchange.com/a/624988/276549)
