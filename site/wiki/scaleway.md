  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">cloud</span>

# Scaleway

## Bucket

Storage is free below 75GO

### Hosting Website

1. create a bucket `blog.parisni.com` (matching the hostname)
1. turn it to public & enable website feature
1. upload the static content with awscli (`aws s3 cp <local/path> s3://blog.parisni.com --recursive`)
1. redirect your domain to the bucket

NOTE: to date, the website cannot benefit from SSL.
