  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">music</span>

# Mpd

## Install


```bash
# the serveur
pacman.install mpd

## the command line client
pacman.install mpc
```

Edit the `/etc/mpd.conf`, in particular add the musique folder.

```bash
# scan the database
mpc update
```


## Use with command line


```bash
mpc search title <my-search>
```


## Use with emacs

```bash
todo
```


## Audio Configuration

https://mpd.fandom.com/wiki/PulseAudio
https://www.musicpd.org/doc/html/user.html#audio-output-format
https://opensource.com/article/17/1/linux-plays-sound
```bash
voir dans nixos
```
