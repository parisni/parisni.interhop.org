  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">firewall</span>
<span class="w3-tag w3-red tag">security</span>

# Iptables

## List every ipv4 rules

```bash
iptables -t nat -L -n -v
```

## Drop rules in specified table

```
iptables -t nat -F
```
