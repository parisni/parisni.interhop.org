  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">network</span>


# Bluetooth on linux

## Blueman
### Setup
[setup bluetooth on ubuntu](https://www.maketecheasier.com/setup-bluetooth-in-linux/)

Then run `blueman-manager` and trust the device.

### Connect a samsung galaxy
1. on the bluetooth tray icon, select *make discoverable*
1. on the phone bluetooth app, select *scan*
1. on the phone gallery, select *share* and then *bluetooth*

## bluetoohctl
### peer a device

[source](https://www.reddit.com/r/i3wm/comments/9wqt4k/connect_to_bluetooth_devices_on_i3/)
```bash
bluetoothctl
$ power on
$ agent on
$ scan on
$ pair <mac address>
$ connect <mac address>
```

### send file to device
[source](https://techiesanswer.com/ubuntu-command-line/unix-bluetoothctl-and-bluetooth-sendto-tools-to-send-file/)
```bash
bluetooth-sendfile
```


