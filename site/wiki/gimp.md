  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">image</span>

# Gimp

## Make some zone be transparent

1. Add gamma to the image
2. select by color
3. press delete

## Underline text

1. Add a layer to the image (if not exit)
2. Choose pencil tool
3. Click
4. Press shift and click on the other extremity
5. You get your underline


## Surround text

1. Choose selection (rectangle or ellipse)
2. Surround
3. Press Enter
4. Select > To path
5. Edit > Stroke path
6. You get your surround

## Change image geometry

1. Choose the "perspective" tool
2. Click ane drag the image as you want
3. Apply by clicking "transform" in the perspective's popup
