
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">filesystem</span>

# Mount
## mtp
mtp is android protocol.

```bash
$ pwd # prints "/run/user/1000/gvfs/mtp:host=%5Busb%3A003%2C096%5D/SanDisk SD card/Movies"

$ rsync --verbose --progress --omit-dir-times --no-perms --recursive --inplace ~/Videos/ ./
```
