  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">security</span>

# Password

So far, password-store is the best option, for cli and browsers.


## Password-sore

It's a bash tool, based on git, gpg, xclip and other linux tools. By combining them,
it inherits the best feature :

- `+`: the cli is very efficient (path navigatation, auto-complete, clipboard expiration)
- `+`: decentralized (each device has the whole)
- `+`: can store almost anything (password, texts, list...)
- `+`: integrates with browsers, android ...
- `-`: not great for team and password sharing (share a private key)


## Vaultwarden

- `+`: great for team and password sharing
- `+`: share media (images, video...)
- `+`: integrates with browsers, android ...
- `-`: centralized architecture (a database/API design), trouble if it gets down
- `-`: has a bad cli (no auto-complete, no clipboard integration)
