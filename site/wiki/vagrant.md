  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">virtulization</span>
<span class="w3-tag w3-red tag">security</span>

# Vagrant

```
vagrant up
vagrant provision
vagrant ssh
vagrant destroy
```

## Libvirt

Kvm is much more efficient than virtual box. It is meant for production worload
with a small resources footprint on the host.

kvm has the `virsh` command line which has many features.

So far, it has no built-in way to deploy from the command line as vagrant does.

The vagrant-libvirt plugin provides the best of two worlds: a great way to
deploy kvm and a great way to manage kvms.

## References

- [vagrant documentatin](https://www.vagrantup.com/docs/synced-folders/basic_usage)
- [vagrant libvirt github](https://github.com/vagrant-libvirt/vagrant-libvirt)
- [use vagrant together with kvm](https://ostechnix.com/how-to-use-vagrant-with-libvirt-kvm-provider/)
- [use vagrant together with kvm bis](https://computingforgeeks.com/using-vagrant-with-libvirt-on-linux/)
- [vagrant ip on host](https://ostechnix.com/how-to-find-vagrant-machine-ip-address-from-host/)
- [install kvm ubuntu](https://phoenixnap.com/kb/ubuntu-install-kvm)
- [kvm delete vm](https://bobcares.com/blog/virsh-command-to-delete-a-vm/)
- [kvm from cli](https://linuxconfig.org/how-to-create-and-manage-kvm-virtual-machines-from-cli)
- [kvm snapshot tutorial](https://www.cyberciti.biz/faq/how-to-create-create-snapshot-in-linux-kvm-vmdomain/)
- [alpine vagrant](https://app.vagrantup.com/generic/boxes/alpine38)
- [convert boxes to libvirt](https://medium.com/@gamunu/use-vagrant-with-libvirt-unsupported-boxes-12e719d71e8e)
