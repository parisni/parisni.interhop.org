  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">coding</span>

# Domain Specific Language

There is many DSL implementation patterns:

- Internal DSL
  - smart API
  - fluent API
  - syntax tree manipulation
  - typed embedding
  - reflective metaprogramming
  - runtime metaprogramming
  - compile time metaprogramming
- External DSL
  - parser combinator
  - mixing DSL and embedded foreign code
  - xml to consumable resource
  

*Internal DSL* are embedded the host programming language while
*Extenal DSL* are created from scratch with in _parse_ / _process_
steps.



## Explicitly typed constraint in scala


