
<div id="refman"> <div id="refman-sidebar"> <div id="generated-toc">
</div>
</div> <div id="refman-main">

<span class="w3-tag w3-red tag">programming</span>

# Python

## Create a cli executable
### Use click
- [use setuptools](https://click.palletsprojects.com/en/8.0.x/setuptools/)

## Environments
### virualenv
it is limited to the python version present on the host linux
distribution. it handles the pip package manager.
### anaconda
anaconda allows to get multiple version of python even if the host
linux distribution does not. it handles both conda and pip package
managers, but conda is the prefered way.
### nix
nix proposes an alternate way of packaging python environments. It is
not compatible with pip.
### guix
it has less packages than nix and has the same drawbacks: python
packages has to be packaged the guix way to be accessible.
### pyenv/direnv
This activates the virtualenv when you go into the folder.

Installation:
```
curl https://pyenv.run | bash

# add to your bashrc
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
```

```
source ~/.bahsrc
curl -sfL https://direnv.net/install.sh | bash
eval "$(direnv hook bash)"
```

Then install the python version you want:
```
source ~/.bahsrc
pyenv install 3.8.11
```

Then in any python project:
```
echo "layout pyenv 3.8.11" > .envrc
direnv allow
```



## Data
### Pandas
### Koalas
### Map/Reduce
### Dask
Dask peut cohabiter avec spark sur un cluster hadoop via
[dask-yarn](https://docs.dask.org/en/latest/). Il peut lire/ecrire sur
hdfs des fichiers parquet/orc.

Le fonctionnement est largement inspiré de spark: dask s'appuie sur la
résolution d'un DAG lazy, avec possibilité de caching des étapes.

La finalité de dask est de transformer des big-data en small-data pour
pouvoir les traiter avec pandas.


## API
### Flask

## Dashboard
### Dash

## Database
### ZODB
[ZODB](https://github.com/zopefoundation/ZODB) uses “object traversal”
to retrieve information from the persistance layer. It supports ACID
transaction and is ideal for complex datamodel.

## Compilation
### numba
[numba](https://github.com/numba/numba)
### cython
