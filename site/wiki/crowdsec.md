  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">security</span>

# Crowdsec

## Install

- crowdsec is in the pacman repos
- crowdsec-firewall is in the aur repos (yaourt)
- it can use postgres as a backend
- prometheus metrics can be vizualized with [graphana dashboards](https://github.com/crowdsecurity/grafana-dashboards)
- also a metabase dashboard exist but can only be used with sqlite
- a nginx-bouncer exists but only for ubuntu at the moment



Create local-api tokens:

```bash
cscli machines add -a
cscli bouncers add  firewall-bouncer
```

Edit the config files :

```
vim /etc/crowdsec/acquis.yaml
vim /etc/crowdsec/user.yaml
vim /etc/crowdsec/config.yaml
vim /etc/crowdsec/bouncers/crowdsec-firewall-bouncer.yaml
vim /etc/prometheus/prometheus.yml
```

Show metrics:

```bash
cscli metrics
```
