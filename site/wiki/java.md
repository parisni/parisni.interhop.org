<div id="refman"> <div id="refman-sidebar"> <div id="generated-toc"> </div> </div> <div id="refman-main">

<span class="w3-tag w3-red tag">programming</span>

# Java

## Heap dump

### Generate a Heap dump


#### On demand

The jcmd tool produce a binary heap dump

```bash
jcmd <pid> GC.heap_dump <file-path>
```

#### On crash

The below option does not add overhead, and using it in production is a good idea.

```bash
java -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=<file-or-dir-path>
```

### Analyse a thread dump

Eclipse Memory Annalyser allows to analyse the resulting dump files. [this tutorial](http://memoryanalyzer.blogspot.com/2008/05/automated-heap-dump-analysis-finding.html) explains well how to use the tool.

