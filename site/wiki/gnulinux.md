  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>

# Gnu-Linux

## Make user change password next connection

```bash
chage -d 0 the_user
```

## Search in the history of command

- `ctrl+r`: reverse search within the history
- `ctrl+s`: search in the history (add `stty -ixon` in the .bashrc)
