  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>

# I3


## D-menu

The

By default, desktop application are located in `/usr/share/applications/` or `.local/share/applications/`:

```ini
[Desktop Entry]
Version=1.0
Name=Qutebrowser
GenericName=Web Browser
Comment=Access the Internet
Exec=qutebrowser %U
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=qutebrowser
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml_xml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
X-AppInstall-Package=qutebrowser
```

Also, the icon can be placed into `.local/share/icons/`

## Lock screen

[xsecurelock](https://github.com/google/xsecurelock) is from far, the best screen locker out there.

and use lock.sh wrapper:
```
#!/bin/sh
XSECURELOCK_SHOW_DATETIME=1 XSECURELOCK_DATETIME_FORMAT="%A, %Y-%m-%d %H:%M" XSECURELOCK_PASSWORD_PROMPT=cursor xsecurelock
```
Also, in order to lock before suspend:

`systemctl enable xsecurelock@parisni`
cat /etc/systemd/system/xsecurelock@.service
```
[Unit]
Description=Lock X session using xsecurelock
Before=sleep.target

[Service]
Type=simple
User=%i
Environment=DISPLAY=:0
Environment=XAUTHORITY=/home/%i/.Xauthority
Environment=XSECURELOCK_SHOW_DATETIME=1
Environment=XSECURELOCK_DATETIME_FORMAT="%%A, %%Y-%%m-%%d %%H:%%M"
ExecStart=/usr/local/bin/xsecurelock

[Install]
WantedBy=suspend.target
```


## Modes

`Alt_gr` is a good candidate for the `mode` key because it is accessible to
the right thumb wich is probably the sronger finger we have.

To be able to set it, one way is to switch both `Alt_R` and `Alt_gr` and set
`$mod = Mod1`.

An other good things is to get the `caps_lock` key do `Escape`, which is
essential in vim/emacs key bindings.

All those changes can be set in the `/etc/default/keyboard` file:

```bash
XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT="altgr-intl"
XKBOPTIONS="caps:swapescape,lv3:lalt_switch,lv3:ralt_alt"

BACKSPACE="guess"
```

And the `~/.config/i3/config`:

```bash
exec_always --no-startup-id '~/bin/xmap'
set $mod Mod1
font pango:monospace 8
floating_modifier $mod
bindsym $mod+n exec i3-sensible-terminal
bindsym $mod+p kill
bindsym $mod+d exec --no-startup-id i3-dmenu-desktop
bindsym $mod+j focus left
bindsym $mod+k focus right
bindsym $mod+Left focus left
bindsym $mod+Right focus right
bindsym $mod+Shift+k move right
bindsym $mod+Shift+j move left
bindsym Control+1 workspace 1
bindsym Control+2 workspace 2
bindsym Control+3 workspace 3
bindsym Control+4 workspace 4
bindsym Control+Shift+1 move container to workspace 1
bindsym Control+Shift+2 move container to workspace 2
bindsym Control+Shift+3 move container to workspace 3
bindsym Control+Shift+4 move container to workspace 4
bindsym $mod+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode
        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym m resize grow width 10 px or 10 ppt
        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt
        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
bar {
        status_command i3status
        tray_output primary
}
exec --no-startup-id nm-applet
bindsym XF86AudioRaiseVolume exec "amixer -q sset Master,0 3+ unmute ; amixer -c 1 -q sset Headset,0 3+ unmute"
bindsym XF86AudioLowerVolume exec "amixer -q sset Master,0 3- unmute ; amixer -c 1 -q sset Headset,0 3- unmute"
bindsym XF86AudioMute exec "amixer -D pulse set Master 1+ toggle ; amixer -c 1 -D pulse set Headset 1+ toggle"
bindsym XF86MonBrightnessUp exec --no-startup-id bash -c 'b=$(xrandr --verbose | grep -i brightness | head -n 1 | cut -d" " -f2) && b2=$(echo -e "import math\nf = lambda x: (1 - 1/(1+math.exp(4*x-2)))\nprint(\\\"%.2f\\\" % f(0.1+$b))" | python3) && xrandr --output eDP-1 --brightness $b2'
bindsym XF86MonBrightnessDown exec --no-startup-id bash -c 'b=$(xrandr --verbose | grep -i brightness | head -n 1 | cut -d" " -f2) && b2=$(echo -e "import math\nf = lambda x: (1 - 1/(1+math.exp(4*x-2)))\nprint(\\\"%.2f\\\" % f(-0.1+$b))" | python3) && xrandr --output eDP-1 --brightness $b2'
exec --no-startup-id xrandr --output eDP-1 --mode 1280x720 --rate 60 --scale 1x1 --dpi 96 --primary

workspace_layout tabbed
popup_during_fullscreen smart
for_window [window_role="pop-up"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [title="Preferences$"] floating enable
for_window [class="(?i)mplayer"] floating enable
set $Locker lock.sh
set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown
mode "$mode_system" {
    bindsym l exec --no-startup-id $Locker, mode "default"
    bindsym e exec --no-startup-id i3-msg exit, mode "default"
    bindsym s exec --no-startup-id $Locker && systemctl suspend, mode "default"
    bindsym h exec "sudo pm-hibernate", mode "default"
    bindsym r exec "sudo reboot", mode "default"
    bindsym Shift+s exec "shutdown now", mode "default"
    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Escape mode "$mode_system"
bindsym Print exec scrot 'Screenshot-nparis_%Y-%m-%d_%H:%M:%S.png'  -e 'mv $f /tmp/ && gimp /tmp/$f'
bindsym --release $mod+Print exec scrot -s  -e 'mv $f /tmp/ && gimp /tmp/$f'
bindsym $mod+i exec diodon
bindsym $mod+x mode "$mode_display"
exec --no-startup-id pasystray --include-monitors -a
for_window [class="jetbrains-idea-ce"] floating disable;

workspace_auto_back_and_forth yes
focus_follows_mouse no
```

## Playing with windows

```bash
xprop
# then click on the application
WM_CLASS(STRING) = "instance", "class"
```

## Tree

```
i3-save-tree --workspace 1 > ~/.i3/workspace-1.json
i3-msg "workspace 1; append_layout ~/.i3/workspace-1.json"
```

## Use external screen

```
xrandr_screen () {
        xrandr --output eDP-1 --off --noprimary && xrandr --output $(xrandr|grep " connected"|tail -n 1| cut -f 1 -d " ") --mode 1920x1080 --rate 75 --scale 1x1 --dpi 96 --primary
}

xrandr_laptop () {
        xrandr --output eDP-1 --mode 1360x768 --rate 60 --scale 1x1 --dpi 96 --primary && xrandr --output $(xrandr|grep " connected"|tail -n 1| cut -f 1 -d " ") --off
}
```
