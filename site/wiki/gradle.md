  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">build</span>

# Gradle 

## Tests specific unit


```
gradle test --tests "package.class.method"
# also
gradle :submodule:test --tests "package.class.method"
```
