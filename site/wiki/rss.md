  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">security</span>
<span class="w3-tag w3-red tag">news</span>

# RSS
## Subscribe to software releases

### Gitlab
```html
https://<theproject>/-/tags?format=atom
```

### Microsoft Github
```html
https://<theproject>/release.atom
```
