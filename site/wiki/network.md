<!--
  <div id="generated-toc"> </div>
  <hr>
  -->

<span class="w3-tag w3-red tag">security</span>

# Network

## Test Port Open

this will test if the remote has port open:
```bash
nc -zv <host> <port>
```

## Test Ports Open


this will scan for open/closed ports on the remote :
```bash
nmap -p 1-10000 <host>
```



