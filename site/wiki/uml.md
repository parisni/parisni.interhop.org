  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">versionning</span>

# UML

## PlantUML

This is FOSS java tool to built UML content from a DSL.

```
@startuml
package "Some Group" {
HTTP - [First Component]
[Another Component]
}
node "Other Groups" {
FTP - [Second Component]
[First Component] --> FTP
}
cloud {
[Example 1]
}
database "MySql" {
folder "This is my folder" {
[Folder 3]
}
frame "Foo" {
[Frame 4]
}
}
[Another Component] --> [Example 1]
[Example 1] --> [Folder 3]
[Folder 3] --> [Frame 4]
@enduml
```

![Component UML](/images/puml.webp)
