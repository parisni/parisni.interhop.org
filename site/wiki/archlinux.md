  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>

# Archlinux

## Aur package

[source](https://linuxhint.com/aur_arch_linux/)

- look for a package on the [aur](https://aur.archlinux.org/)
- clone it `git clone https://aur.archlinux.org/<pkgName>.git`
- build it `makepkg`
- install it `makepkg -sri`
- optional: delete it with pacman
