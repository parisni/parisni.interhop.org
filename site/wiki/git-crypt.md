  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">versionning</span>
<span class="w3-tag w3-red tag">security</span>

# Git-Crypt

First of all: install [git-crypt](https://www.agwa.name/projects/git-crypt/)

## Initialize an encrypted repository
[see documentation](https://github.com/AGWA/git-crypt#using-git-crypt)

## Work on a encrypted repository
Either **import a gpg key** ...:
```bash
git clone <the repos>
cd <the repos>
git unlock
```


...or use the **symetric key** (see below):
```bash
git clone <the repos>
cd <the repos>
git unlock /path/to/key
```


## Export a gpg key
```bash
gpg --export-secret-keys YOUR_ID_HERE > private.key
```

## Import a gpg key
```bash
gpg --import private.key
gpg --edit-key YOUR_ID_HERE
# type “trust”
# type “5”
# type “save”
```

## Export a symetric key
```bash
git-crypt export-key /path/to/key
```
