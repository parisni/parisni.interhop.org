  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">scala</span>
<span class="w3-tag w3-red tag">web</span>

# pdf
## pdf viewer

I have for long time being disapointed with **evince** pdf reader with
it's lack of capacity to zoom in and out. The same happened with
**foxit reader** and its lack of history management in its linux
version.

Finally I discovered **mupdf**. It has a vi key biding, has a great
zooming window, allows back in the history. The only missing feature
is beeing able to follow links with keyboard as **luakit** does.


### keybinding

- *i* : dark mode
- */* : search
- *W* : fit to page
- *t* : go back to last jump

## Concatenating pdf

The command below is great to concat multiple pdf together. I had some
problem with pdftk.

```bash
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=merged.pdf mine1.pdf mine2.pdf
```

Also this command allows to compress a large pdf into something
smaller:

```bash
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages -dCompressFonts=true -r150 -sOutputFile=output.pdf input.pdf
```

It is also possible to split pdf with pdfbox:

```bash
java -jar /app/edsr/script/java/pdfbox-app-2.0.0-SNAPSHOT.jar PDFSplit ./authorisation-inscription.pdf
```

## pdf generation
### from html
weasyprint makes a brillant work to transform html page into pdf.
```bash
pip3 install weasyprint
weasyprint https://website/index.html index.pdf
```
