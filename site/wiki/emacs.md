<div id="refman"> <div id="refman-sidebar"> <div id="generated-toc">
</div>
</div> <div id="refman-main">

<span class="w3-tag w3-red tag">editor</span>

# Emacs
## Comparaison de deux fichier

Une manière simple de comparer deux fichiers est la commande
`compare-windows`. Il faut avoir ouvert deux fenetres avec chacune son
fichier. Par la suite, on peut accéder aux différences en re-lancant
la commande `compare-windows` à plusieurs reprises, jusqu´à ce que le
processus soit satisfaisant --- ou terminé.


## Gestion git avec Magit
``` bash
c c : commit
P p : push sur la meme branche
F p : pull sur la meme branche
l -- <fichier> l : afficher tous les commit d'un même fichier
ll ri: rebase interactif
```

## Completion des chemins
helm offre une completion de `path`. A condition de précéder par un
guillemet. par la suite, la navigation se réalise via les touches
`readlines`, y compris `C-j`

## Utiliser tramp pour editer des fichiers distants
Editer des fichiers distants via emacs est interessant:
- utiliser ses configuration
- bénéficier de helm pour la navigation d´arborescence
- emacs n´est pas installé par défaut sur des serveurs distants

```lisp
<C-x> <C-f> ;;puis
/ssh:<user@host>:<remote-path>
```

On peut aussi rebondir sur plusieurs remotes:
```lisp
/ssh:user@remotehost|ssh:user2@remotehost2:/path
```

## Éditer le contenu des dossiers avec Dired
Lancer le mode dired dans un dossier:

Les principales commandes sont [listées ici](https://stackoverflow.com/questions/5220344/postgresql-invalid-page-header-in-block).

```lisp
<C-x d>, then choose the folder
<C-x C-q> or i, turn editing mode on
```

Then you can :
- delete a file with `D`
- batch rename files `:s/foo/bar`

[Apparently, "rsync" method is more robust than ssh](https://stackoverflow.com/a/10661439)

The bellow code allows to open multiple files:
```lisp
(eval-after-load "dired"
  '(progn
     (define-key dired-mode-map "F" 'my-dired-find-file)
     (defun my-dired-find-file (&optional arg)
       "Open each of the marked files, or the file under the point, or when prefix arg, the next N files "
       (interactive "P")
       (let* ((fn-list (dired-get-marked-files nil arg)))
         (mapc 'find-file fn-list)))))
```

## Gestion des layout windows
La sauvegarde / restauration est perdue si la session emacs et terminée.

### Sauvegarder
```lisp
<C-x r w> <la lettre du layout>
```

### Restaurer
```lisp
<C-x r j> <la lettre du layout>
```

## Terminaux
`vterm` s´impose comme le terminal emacs le plus complet et robuste.


## Remote color
Pour avoir des couleurs 24bit sur un remote terminal on peut appliquer
[cette méthode](https://stackoverflow.com/a/50577683)

## Packages
### window-numbering

This package allows to identify window with number and jump to any
with `<M-[1-9]>`. This is far better than the default `<C-x o>`


## Spelling
- `M-x flyspell-mode`: souligne les mots du document
- `M-x ispell-change-dictionnary`: propose de choisir un autre dictionnaires

```bash
# installation sous archlinux
# les paquets
aspell-fr aspell-en
```
## Python
A complete overview [can be found here](https://realpython.com/emacs-the-best-python-editor/)

### elpy
Allows to run portion of code inside a python repl. Interesting command:
- `elpy-test-pytest-runner`: run pytest on cursor or every tests
- `elpy-autopep8-fix-code`: fix code formatting accoring to `tox.ini`  file an pep8 standard
- `jedi:goto-definition`: show the source of the python object under the cursor

### Pyvenv
On peut charger un environnement **venv** ou **conda** via `M^X
pyvenv-activate` et spécifier:
- le chemin du virtualenv
- `anaconda3/envs/<non venv>`

```bash
M-x pyvenv-activate
# then choose the venv root dir
```

### jedi
```bash
jedi:setup
```


### realgud
[realgud](https://github.com/realgud/realgud/) allows to debug python
and also place breakpoints thanks to the python `pdb` module

- `F9`: add breakpoint
- `F10` : go to next
- `D`: remove all breakpoints

## Treat underscore as word

This will allow to treat underscore or dash, depending on the
programming context as words - only in case of pressing `#` or `*`

```lisp
(with-eval-after-load 'evil
    (setq-default evil-symbol-word-search t))
```

## Usage of frame

Multiple frames can be created. Navigation between them is `C-x 5
o`. Each frame can have its own layout. That's great, for example to
have magit in an other frame.
