
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">social</span>

# IRC

## Weechat

### Search for room

```
/search -re <the regex>
```

### Join a room

```
/join #foo
```


### Quit a room

```
/part 
```

### Short Keys

```
<ctrl+x>: change server
<ctrl+n>: move down room
<ctrl+p>: move up room
```
