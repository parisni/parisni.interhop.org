  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">linux</span>

# Systemd

## Systemd-run

Run with the current user environment, with cgroup quota :

```bash
systemd-run --user  --scope sh -c 'echo hello ; sleep 10 ; echo $(pwd)'
systemd-run --user -pCPUQuota=200%  --scope sh -c 'echo hello ; sleep 10 ; echo $(pwd)'
```


## Journalctl

### Filter
It centralizes all the systemd logs. You can filter by many criteria. It has a
handy command completion:

```bash
journalctl _COMM=prometheus --since '2021-11-01 11:00' --until '2021-11-01 12:00'
```

### Relative time filter

```bash
journalctl _COMM=prometheus --since -1h
```

### Follow
This allows to see incoming logs:

```bash
journalctl _COMM=sshd -f
```

### Kernel
```bash
journalctl -k
```

### Boot
```bash
journalctl -b
```

### Priority

```text
0: Emergency. The system is unusable.
1: Alert. A condition has been flagged that should be corrected immediately.
2: Critical. This covers crashes, coredumps, and significant failures in primary applications.
3: Error. An error has been reported, but it is not considered severe.
4: Warning. Brings a condition to your attention that, if ignored, may become an error.
5: Notice. Used to report events that are unusual, but not errors.
6: Information. Regular operational messages. These do not require action.
7: Debug. Messages put into applications to make it easier for them to debug them.
```

## Edit systemd files

The system files are located in `/usr/lib/systemd/system` and are overwriten when the system get updated.

There is two options to make your own unit:

1. create a systemd file in `/etc/systemd/system`: it will overwrite the system's one
2. run `systemctl edit <service-name>`: it will add content into
   `/etc/systemd/system/<service>.service.d/override.conf`. Note that it does not
   need daemon-reload and can only append config rows.
