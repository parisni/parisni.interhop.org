
  <div id="generated-toc"> </div>
  <hr>

<span class="w3-tag w3-red tag">device</span>

# Keyboard

## Layout us variant intl
[all the keys](https://dry.sailingissues.com/us-international-keyboard-layout.html)

- é : `altgr+e`
- á : `altgr+a`
- « : `altgr+[`
- » : `altgr+]`
- œ : `altgr+k`
- æ : `altgr+z`
- ' : `altgr+'`
- ç : `altgr+,`

## Change the layout

```bash
$ cat /etc/default/keyboard
# KEYBOARD CONFIGURATION FILE

# Consult the keyboard(5) manual page.

XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT="intl"
XKBOPTIONS=""

BACKSPACE="guess"
```

then `systemctl restart keyboard-setup`
