.. title: Spark interactions with Postgres
.. slug: spark-interactions-with-postgres
.. date: 2019-08-24 00:11:34 UTC+02:00
.. tags:  spark, postgresql
.. category: data engineering 
.. link: 
.. description: This present the spark-postgres library
.. type: text

.. raw:: html

	 <span>Date:<time id="post-date">2019-08-24</time></span>
	 <p>Estimated Time:<span id="reading-time"/></p>

Spark interactions with Postgres
================================

Both spark and sqoop allow nativelly to ingest/load relational
databases such postgresql. After being disapointed by both, I have
finally developped an optimized library to better bridge spark and
postgres: reading from postgres, writing to postgres from spark.


The spark-postgres library
==========================

Comparison with Sqoop
======================

Automation with yaml
====================
