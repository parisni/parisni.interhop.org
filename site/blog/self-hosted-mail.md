

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2021-06-13</time></span>
<span id="reading-time" style="visibility:hidden;"/>

# Self hosting emails in the 21th centory

## Why self hosting emails

Mailing technology still active after decades of production. It's one
of the simpliest and universal way of sharing information. There is
out there a lot of very efficient solutions such gmail or
protonmail. 

But mail is FUN with a very broad spectrum of technology to
master. For example, mailu is built over containers and targets
kubernetes, it has a flask api/cli, is maintained by a prolific
community using great automation tools on the continuous delivery and
also bots on matrix. It has a machine learning based spam filter.

Also mail is serious with personal informations, that shall be kept
secret. Hacking ofter takes place on the mailing area, and
understanding how this stuff works is a great skill.

## Available mailing selfhosted solutions

Mail In A Box (aka MIAB) looks the most advanced solution.


Mailu is quite new and it naturally comes with less features. One
major distinction compared to MIAB is its configurability. Most of the
modules are optional or sereval choices availables.

## Ham & Spam

The `smtp port 25` shall be opened. However most internet box close it
by defaul. It is often possible to open it from the box web ui.

The `reverse dns`is a huge obstacle to self host email. The domain
name associated with the home ip server has to be the same as the mail
domain name. Sadly most internet providers does not allow clients to
change the value of rDNS (which is trivial on the cloud).

## Using a relay

Because sending trusted email from a home server is so tricky one can
use a mail provider (such riseup) to deliver the email. Also the
provider can route the email to your selfhosted server. In this case,
you still use your existing email, but nothing is stored on the
provider side but on your own instance. 

The main advantage with this is you manage your storage yourself. Also
in term of privacy, your provider does not store your email
history. By the way the email only transit on his side.

## Email client

I have been using in the past multiple cli email clients such `mutt`,
`neomutt` and `mu4e` together with local indexing tools such `notmuch`
or `mu`. Those tools are not designed to work with imap but with
pop. This means the emails are pulled locally and indexed.

Having a self-hosted email solution such `mailu` or `mail in a box`
let use the dovecot full text searching feature based on xapian or
solr. Together with the imap protocol, it leverages server search with
great performances. Moreover imap is far better than pop because all
your client will have the same view on the maildir, and there is no
need to store locally the emails which improves security.

In the terminal, `aerc` is a great cli email client focused on imap
and it supports server side FTS (`filter -a`).  

On desktop, evolution also is a great software, and it provides imap
push in order to get real time notifications and decrease server load.

On mobile, `k9mail` is great, and provides draft email encryption,
autocrypt. Sadly the FTS is not yet supported. I would be glad to help
on this. I have also tested fairemail, but most interesting features
are limited to the paid version.

## Calendar, contacts and tasks

Having our contacts and calendar together with the email is
important. Mailu provides a simple python package `radicale` which
brings this features. It does not come with a visual calendar, but can
export vcal and ics formats. Mobile applications such `etar calendar`,
`tasks`, can connect to radicale and synchronize content thought `davx5`.

Mail-in-a-box comes with next-cloud calendar which is a more complete
solution but too heavy for my taste.

Roundcube webmail is able to connect to radicale carddav feature
only. Sadly it does not provide any decent calendar plugin.
