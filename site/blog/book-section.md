<div id="refman">

<div id="refman-sidebar">
  <div id="generated-toc"> </div>
</div>
<div id="refman-main">

<span>Published on <time id="post-date">2020-03-08</time></span>
<span id="reading-time" style="visibility:hidden;"/>

# Introducing the &quot;Readings&quot; section


### Principle

Books are meant to be __read__. Also books should be __reviewed and
criticized__. I am introducing a new book section with several goals
in mind:
- read more books
- read carefuly
- keep notes, and summary

### Content

Books, comics, novel, essay, philosophy and also research papers will be covered.
In term of content, I can imagine:
  * reference
  * citation
  * synthesis
  * form analysis
  * and also cover picture
  
I wonder I it's legal to share citations, and pictures of comics. This
is something I will check out.

### Form

I guess I could distinguish several categories, and provide __views per
categories__. 

#### Entry preview
	
Each entry preview would have:
  * title
  * summary
  * thumbnail
  * stars
  
#### Entry content

Depending on the category, each book entry might have:
- summary
- screenshots
- citations

### Next Steps

I will need to explore existing things on the web. Still the content
and reading can start with this goal. I can think of:
- [raw meat by aaronsw](https://qblog.aaronsw.com/page/7)
- [pinboard](https://pinboard.in/u:aaronsw/)
- [raw thought](http://www.aaronsw.com/weblog/)
- [timeline](https://codepen.io/pun-isher/pen/qjZaXN)
