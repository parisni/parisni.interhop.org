.. title: Best terminal Emulator
.. slug: termite
.. date: May 27, 2018
.. link:
.. description:
.. tags: termite
.. category: tools


.. raw:: html

	 <span>Date:<time id="post-date">2018-05-27</time></span>
	 <p>Estimated Time:<span id="reading-time"/></p>

Best terminal Emulator
----------------------

Termite is way better than my previous terminals. It's sensibly based on vim
paradigm. It's able to look ahead in the history, copy from it and back to
insert mode.

.. END_TEASER

An other great aspect is it notifies when something happens in it, such end of
process, new mail an so on.


keybinding
**********

- *ctrl+space* : insert mode
- *escape* : normal mode
