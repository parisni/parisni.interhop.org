
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-09-24</time></span>
<span id="people-birthyear" style="visibility:hidden;">1943</span>
<span id="people-deathyear" style="visibility:hidden;"></span>
<span id="people-profession" style="visibility:hidden;">Programmer</span>
<span id="reading-time" style="visibility:hidden;"/>


# Alvy Ray Smith

- [Alvy Ray Smith](http://alvyray.com/)
