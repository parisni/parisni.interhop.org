
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2022-01-01</time></span>
<span id="people-birthyear" style="visibility:hidden;">1958</span>
<span id="people-deathyear" style="visibility:hidden;"></span>
<span id="people-profession" style="visibility:hidden;">Activist</span>
<span id="reading-time" style="visibility:hidden;"/>

# Didier Lestrade

Fondateur d'Act Up-Paris (inspiré de celle de New-York), il relate :

> on peut parler pendant une heure, mais si ça ne débouche pas sur une action, alors 
> c'est une discussion qui a servit a rien

> on considérait que les gens devaient s'accrocher. si tu ne fous rien, tu ne m'intéresses pas. Si tu ne fais pas 
> un effort pour apprendre, pour lire ceci, pour écrire cela, pour participer t'es un poids mort. à un militant 
> qui arrivait, on demandait "qu'est-ce que tu veux faire ? ou que sais-tu faire ?"

> je leur dis aux écolos, vous êtes trop gentils, il faut arriver maintenant à
une confrontation physique. Aller chez eux, terroriser leurs enfants avec des
casseroles, bloquer leurs téléphones, faire réellement pression. [...] on se
retrouve comme pour le sida face à des multinationales encore plus puissantes,
mais on connait toujours leur point faible. Il faut tapper sur le point faible.
