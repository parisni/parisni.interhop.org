## Welcome  !!
![abominable homme des neiges](/images/abom_h.gif)

This is my personal homepage. This is an experimental place to testify
how software can lead to lobotomy.

## Why Software would act as Lobotomy ?

As far as I can remember, computers have been captivating for
me. Captivating -- not fascinating. The same situation as the Sirens
and Ulysses.

## About the current website

I am currently using [Soupault](https://github.com/dmbaturin/soupault)
as a weblog site management and very happy with it. The tool reconcils
simplicity and power. I can't imagine the limit of the creativity it
provides. And that's why I am so enthusiast with this.

## Website Activity

<img src="images/activity.webp"
     alt="Activity on the blog"
     style="width:70%" />

<div id='noreact'/>
