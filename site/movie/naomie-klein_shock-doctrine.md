

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-08-05</time></span>
<span id="book-year" style="visibility:hidden;">2015</span>
<span id="book-author" style="visibility:hidden;">Naomi Klein</span>
<span id="reading-time" style="visibility:hidden;"/>


# La stratégie du choc / Capitalisme du désastre
Le documentaire reprend l´histoire des 60 dernières années sous le
prisme de la stratégie du choc tel que pensé par Naomi Klein dans son
livre 2007 du même nom.

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/23e98fc1-f862-48d2-a0d9-1ed17a2d56fe" frameborder="0" allowfullscreen></iframe>

## La torture par le choc
En 1951 un programme de recherche sur la privation sensorielle financé
par l´armé a débuté. Ils se sont rendu compte que ces privations sont
plus dures à supporter que les douleurs physiques ou morales. Le
psychiatre [Donald
Cameron](https://en.wikipedia.org/wiki/Donald_Ewen_Cameron) s´est
livré à des expériences sur les électrochocs, et martèlements
audio. Nombreuses expériences menées à cette époque sont reprises dans
le [manuel d´interrogatoire de la
CIA](https://en.wikipedia.org/wiki/U.S._Army_and_CIA_interrogation_manuals#CIA_manuals)

## La dérégulation financière par le choc
Par ailleurs [Milton
Friedman](https://en.wikipedia.org/wiki/Milton_Friedman) construisait
sa théorie comme quoi une thérapie de choc devait être appliquée à
l´économie pour faire accepter à la population les dérégulations.

## Le Chili
Après des décennies de démocratie aux Chili, [Salvator
Allende](https://en.wikipedia.org/wiki/Salvador_Allende) décida de
nationaliser de nombreuses société y compris celle de la téléphonie
dans laquelle [Richard
Nixon](https://en.wikipedia.org/wiki/Richard_nixon) avait des
intérêts. La CIA mis le [Général
Pinochet](https://en.wikipedia.org/wiki/Augusto_Pinochet) au pouvoir
pour appliquer la dérégulation économique qui se solda par une
inflation inédite dans un pays jusqu´à présent prospère.

## L´Argentine
[Général Videla](https://en.wikipedia.org/wiki/Jorge_Rafael_Videla)

## L´UK & les USA
Dans les années 80 deux disciples de Friedman arrivent au pouvoir dans
deux pays anglo-saxons: [Margaret
Thatcher](https://en.wikipedia.org/wiki/Margaret_Thatcher) et [Ronald
Reagan](https://en.wikipedia.org/wiki/Ronald_Reagan).

En Angleterre, Thatcher fera plier les syndicats des mineurs après un
ans de bras de fer. Puis pour augmenter sa cote de popularité en
dégringolade, elle engagera son pays dans une guerre à l´autre bout du
monde [aux
Malouines](https://fr.wikipedia.org/wiki/Guerre_des_Malouines). Des
mesures économiques suivirent ce plébiscite: privatisation de nombreux
secteurs de l´économie, gaz, électricité, aéronautique, téléphonie...
On parle de
[Big-Bang](https://en.wikipedia.org/wiki/Big_Bang_(financial_markets))
économique.

Aux USA, Reagan, était accompagné de disciples de Friedman tel que
[Donald Rumsfeld](https://en.wikipedia.org/wiki/Donald_Rumsfeld).

## L´URSS

Boris Yeltsin,

## L´Irak
