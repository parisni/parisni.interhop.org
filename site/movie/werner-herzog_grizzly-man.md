<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-09-22</time></span>
<span id="book-year" style="visibility:hidden;">2005</span>
<span id="book-author" style="visibility:hidden;">Werner Herzog</span>
<span id="reading-time" style="visibility:hidden;"/>

# Grizzly Man

Le documentaire reprend des séquences réalisée par [Thimothy
Treadwell](https://en.wikipedia.org/wiki/Timothy_Treadwell) auprès des
Grizzly qu'il fréquenta pendant 13 étés. Acteur raté, il était devenu
une célébrité en réalisant ces films engagés pour la cause des ours et
et des animaux sauvages.

Hersog compare la démarche de Tim à celle de
[Henry David
Thoreau](https://en.wikipedia.org/wiki/Henry_David_Thoreau) ou encore
de [Chris McCandless](https://en.wikipedia.org/wiki/Chris_McCandless)
; un rejet de la société humaine.

Anectote amusante, l'ours qui dévora Thimothy portait le tatouage
n°141. Le bus dans lequel Chris termina ses jours portait le n°142.


Une cassette a enregistré les derniers moments de Tim et de sa
compagne. En ayant écouté une partie Herzog prétendra n'avoir jamais
entendu rien de plus effroyable.
