<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-07-19</time></span>
<span id="book-year" style="visibility:hidden;">1977</span>
<span id="book-author" style="visibility:hidden;">Werner Herzog</span>
<span id="reading-time" style="visibility:hidden;"/>

# Fitzcarraldo

En Amazonie, Fitzgerald a monté une entreprise de voie ferrée qui
périclite par manque de besoin et par la difficulté de la tâche. Par
la suite, il monte un entreprise de fabrique de glaçons par des
procédés chimiques. Il est associé à Mme Mollie. qui tient un bordel et par
ce biais lui permet de financer ses fantaisies.

Pour son troisième projet, il décide de démarrer une plantation
d´hévéas en plein coeur de l´amazonie dans un domaine sous contrôle
des indiens Jivaros - réputés très dangereux et qui réalisent des
réductions de têtes.

F. exploite le mythe des indiens Jivaros selon lequel un dieu blanc
les guidera vers un lieu dénué de souffrances.  Il répond aux flèches
des indiens par l´opéra de Verdi. Les Jivaros sont impressionnés par la
jonque puis par la dynamite et la glace.

Afin d´atteindre le domaine d´exploitation des hévéas, il leur faut
atteindre un fleuve et franchir des rapides qui réduiraient en miettes
la jonque. F. décide de faire passer le navire par dessus la colline
"comme une vache passe par dessus la ferme".

Après avoir terrassé la colline à la dynamite, par un système à base
de nombreuses poulies et avec l´aide de centaines de Jivaros, ils
parviennent à faire passer la jonque sur l´autre bras du fleuve.

À la faveur de la pleine lune et de l´ivresse de la fête qui s´ensuit,
les Jivaros décident de sacrifier le "char sacré" pour apaiser les
esprits des indiens morts durant la manoeuvre. Ils se retrouvent
finalement à leur point de départ. Le bateau est revendu à son
propriétaire. Mais avant, ils ont l´occasion de jouer un opéra de
Verdi par Caruso dessus.


Deux ans plus tard, [Apocalypse
Now](https://fr.wikipedia.org/wiki/Apocalypse_Now) sortait. On peut
dire que les grands moments de l´opéra de Wagner dans les hélicoptères
et la traversée du fleuve sur la frégate et l´attaque des indiens est
largement inspirée de ce chef d´oeuvre.
