

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-06-08</time></span>
<span id="book-year" style="visibility:hidden;">2020</span>
<span id="book-author" style="visibility:hidden;">Cédric Tourbe</span>
<span id="reading-time" style="visibility:hidden;"/>

# Les bourreaux de Staline: Katyn 1940

> Il ne tire pas. Il fait son travail.


Le documentaire revient sur un évènement qui a traversé la seconde
partie du 20ième siècle. L´avenement de Staline s´est construit sur
des pratiques de purges et de massacres depuis les années 1920. Par
exemple, on estime les grandes purges ont supprimé près de 30% de la
population. Ou alors en Ukraine, la famine forcée fera 4 milions de
morts, lors de la [collectivisme de l´agrigulture](https://fr.wikipedia.org/wiki/Famines_sovi%C3%A9tiques_de_1931-1933) en Ukraine.

Quand 20 ans plus tard, les soviétiques annexent la pologne
conformément aux accords secrets du traité de non-aggression
germano-soviétique, c´est tout naturellement qu´ils vont exécuter à
bout portant 20.000 officiers polonais dans le plus grand secret, puis
en accusant l´Allemagne Nazie.

Pourtant, Churchill aura tous les éléments 3 ans plus tard faisant
accuser les Bolcheviques. Mais plutot que de froisser Staline à un
moment crucial de la guerre, il taira l´information.


# Un régime fondé sur le crime

Ce documentaire ancre le récit dans l´histoire et ce relief contraste
la terreur mise en place à tous les échellons de parti
bolchévique. Celui qui tuera par le parti, périra par le parti.


<div>
<iframe width="100%" height="415"  sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/9393c109-df7b-4b78-b6e5-8f88f4f7b11d" frameborder="0" allowfullscreen></iframe>
</div>
