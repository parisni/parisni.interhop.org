
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-07-14</time></span>
<span id="book-year" style="visibility:hidden;">2018</span>
<span id="book-author" style="visibility:hidden;">Martin Odersky</span>
<span id="reading-time" style="visibility:hidden;"/>

# Functional program design in scala


Thee [cheat sheet](https://courseware.epfl.ch/courses/course-v1:EPFL+progfun2+2018_T1/courseware/651f1b406e1d44a59c791a2c2e60e8a6/8bfe81c280ee467a8097589f70074e3d/1?activate_block_id=block-v1%3AEPFL%2Bprogfun2%2B2018_T1%2Btype%40vertical%2Bblock%40f5ed015f512645589d85970514876c5f) recaps the course.


# For expression and monads

[Monads](https://www.youtube.com/watch?v=7aINDO1LgHI)

for expression can always be translated into high order functions
(map, flatmap, filters...)

A monad is a type which implement flatMap and a unit.

```scala
def flatMap[U](f: T => M[U]): M[U]
def unit[T](x :T): M[T]
```

Moreover a monad must conform to 3 laws:

- associativity.

```scala
m flatMap f flatMap g == m flatMap (x => f(x) flatMap g)
```

- left unit


```scala
unit(x) flatMap f == f(x)
```

- right unit

```scala
m flatMap unit == m
```
