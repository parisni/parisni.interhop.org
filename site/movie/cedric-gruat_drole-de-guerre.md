
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-09-24</time></span>
<span id="book-year" style="visibility:hidden;">2018</span>
<span id="book-author" style="visibility:hidden;">Cédric Gruat</span>
<span id="reading-time" style="visibility:hidden;"/>

# La drôle de guerre

Le documentaire revient sur la “Drôle de Guerre” telle qu'elle fut
nommée assez tôt. Il tente le difficile exercice d'offir un regard
objectif en faisant abstraction de l'issue. Pour comprendre ce que
l'Histoire rend _incompréhensible_.

« Depuis pas mal de temps nous ne faisions que nous débattre dans un rêve. Nous révions la guerre et la paix. Jamais l'Histoire n'avait parut aussi loin de ceux qui la font. C'est cela le propre de notre éṕoque : d'avoir profondément désorganisé le réel. De nous avoir fait perdre notre confiance dans les choses et les êtres. Les machines s'en sont mêlées: la TSF, le cinéma, le téléphone... Toutes ces machines inventées pour nous soustraire au contact direct. C'est peut-être pour cette raison là qu'on est perdus dans l'évènement. Pour celle-là ou d'autres, d'ailleurs qu'est-ce que ça fait ? Est-ce que je vais me mettre moi aussi à inventer des esplications ? On y comprend rien, voilà tout. » [Georges Hyvernaud](https://fr.wikipedia.org/wiki/Georges_Hyvernaud)
