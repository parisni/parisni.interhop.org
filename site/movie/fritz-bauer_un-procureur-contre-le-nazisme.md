

<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-09-06</time></span>
<span id="book-year" style="visibility:hidden;">2020</span>
<span id="book-author" style="visibility:hidden;">arte</span>
<span id="reading-time" style="visibility:hidden;"/>

# Fritz Bauer, un procureur contre le nazisme

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/7985bd4c-304d-460c-8621-e0547f9ef54e" frameborder="0" allowfullscreen></iframe>


Le procureur général juif Fritz Bauer a mené les rares procès allemands
contre les nazi à l'après guerre. Il a eu de nombreuses difficulté et
son entreprise n'a pas été un succès à la mesure de l'enjeu.

Il lui aura fallu patienter 4 ans avant de pouvoir revenir en
Allemagne sous prétexte que les allemands n'étaient pas prêts à avoir
des cadres juifs sitôt la fin de la guerre.

La majorité de ses collègues procureurs avaient collaboré et soutenu
les injustices du régime nazi et il pouvait compter sur eux pour lui
mettre des battons dans les roues. C'est pour cette raison qu'il donna
Eichmann aux israéliens et que le procès ne se passa donc pas en
Allemagne, privant le pays d'un débat nécessaire. Il devra d'ailleurs
s'éteindre alors qu'il initiait un procès contre ses pairs.

Le procès d'Auschwitz qu'il mena en 1964 tout de même fut une tribune
et l'occasion pour de nombreux allemands de découvrir l'horreur des
camps, ainsi que la banalité des coupables. De manière intéressante,
aucun d'eux ne se repentirent ou avouèrent leur culpabilité malgré
leur participation active dans les camps. En règle générale, lors des
génocides, les auteurs ne se sentent pas responsables de leurs actes
et invoquent leur devoir auprès de la hiérarchie. On peut cependant
citer Albert Speer, ministre de l'armement de Hitler qui s'il n'a pas
admit avoir été au courant des massacres dans les camps (ce qui s'est
avéré être un mensonge) a tout de même reconnu l'horreur et son
repentir d'avoir participé indirectement à ces crimes.
