
<div id="generated-toc"> </div>

<span>Published on <time id="post-date">2020-10-02</time></span>
<span id="book-year" style="visibility:hidden;">1975</span>
<span id="book-author" style="visibility:hidden;">Marguerite Duras</span>
<span id="reading-time" style="visibility:hidden;"/>

# India Song

Le vice consul des Indes [Mickael
Lonsdale](https://fr.wikipedia.org/wiki/Michael_Lonsdale) succombe à
l'éblouissante [Delphine
Seyrig](https://fr.wikipedia.org/wiki/Delphine_Seyrig). Ce qui est
extraordinaire est que la réalité dépasse la fiction puisque
M. Lonsdale étaient réellement fou de D. Seyrig, amour non réciproque
qu'il n'oubliera jamais. Il restera seul toute sa vie.

[Bruno Nuytten](https://fr.wikipedia.org/wiki/Bruno_Nuytten) est le
directeur de la photographie. Il le fut pour “les valseuses” la
“la meilleure façon de marcher” ou encore “Manon des sources”.

![Lahore c'est moi](/images/india-song/green.webp)
![Lahore c'est moi](/images/india-song/breath.webp)
![Lahore c'est moi](/images/india-song/bike.webp)
![Lahore c'est moi](/images/india-song/smile.webp)
![Lahore c'est moi](/images/india-song/sea.webp)

```text
Il s'est produit un déchirement de l'air. Sa jupe contre les arbres. Elle m'a regardé:
- je ne savais pas que vous existiez. Calcutta est devenu pour moi une forme de l'espoir.
- j'aime Mickael Richardson. Je ne suis pas libre de cet amour.
- je le sais. je vous aime ainsi, dans l'amour de Mickael Richardson. Ça ne m'importe pas. Je parle faux. Vous entendez ma voix ? Elle leur fait peur.
- oui
- de qui est elle ? J'ai tiré sur moi à Lahore sans en mourir. Les autres me séparent de Lahore. Je ne m'en sépare pas. Lahore c'est moi. Vous comprenez aussi ?
- oui, ne criez pas.
- oui. Vous êtes avec moi devant Lahore. Je le sais. Vous êtes en moi. Je vous emmenerai en moi. Et vous tirerez avec moi sur les lépreux de Shalimar. Qu'y pouvez vous ? Je n'avais pas besoin de vous inviter à danser pour vous connaître. Et vous le savez.
- je le sais.
- il est tout à fait inutile qu'on aille plus loin, vous et moi. Nous n'avons rien à nous dire. Nous sommes les mêmes.
- je crois ce que vous venez de dire.
- les histoires d'amour vous les vivez avec d'autres. Nous n'avons pas besoin de ça. Je voulais connaître l'odeur de vos cheveux. C'est ce qui vous explique que j... après la réception, vous restez entre intimes. je voudrais rester avec vous une fois.
- vous n'avez aucune chance.
- il me chasseraient.
- oui, vous êtes quelqu'un qu'il leur faut oublier.
- comme Lahore?
- oui
- que vais-je devenir ?
- vous serez nommé loin de Calcutta.
- c'est ce que vous, vous désirez ?
- oui
- bon. et quand cela va t'il finir ?
- avec votre mort je crois
- quel est ce mal. le miens ?
- l'intelligence
- de vous ? je vais leur demander qu'ils me gardent ici ce soir.
- faites comme vous voudrez
- pour que quelque chose ait lieu entre vous et moi. un incident public. je ne sais que crier. et qu'ils le sachent au moins qu'on peut crier un amour. je le sais ils seront mal à l'aise. et puis ils recommenceront à parler. je sais même que vous ne direz à personne que vous étié d'accord.
```
